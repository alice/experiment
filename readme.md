# Exerimental Fiber app

## Compile And Run

```
git clone https://codeberg.org/alice/experiment

cd experiment
```

```js
npx fiberism serve
```

To create a persistant build, use

```js
npx fiberism compile
```
