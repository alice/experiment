# User Store

This store holds metadata about the user, namely whether they've used the app
before. This data is stored persistently on their local device.

## Imports

```js
import { createStore } from '../framework/framework.js'
```


## Setup

```js
const UserStore = {
    state: {},
    mutations: {}
}
```


## State

Initially, the user's `isFirstVisit` state is set to `true`. This later changed
to `false` after they've viewed the [welcome slides](../routes/Welcome.component.md).

```js
UserStore.state.isFirstUse = true
```


## Mutations

The only mutation needed is a settter for `isFirstUse`. Note that `!!value`
ensures the updated state is a Boolean.

```js
UserStore.mutations.negateFirstUse = function(state) {
    state.isFirstUse = false
}
```


## Persistence

By definition, `isFirstUse` needs to persist accross browsing sessions.


## Exports

```js
export default createStore.persist('user', UserStore)
```
