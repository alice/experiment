# Preferences Store

This store is used to hold user preferences. Currently, this just includes the
app's theme (light or dark).

## Imports

```js
import { createStore } from '../framework/framework.js'
import settings from '../settings.js'
```


## Setup

Create a new store object:

```js
const PreferencesStore = {
    state: {},
    mutations: {}
}
```


## State

Initially, the theme is set to whatever the default is in [fiber.yaml](../fiber.yaml).

```js
PreferencesStore.state.theme = settings.theme.default
```


## Mutations

The only mutation is toggling/switching the current theme. The function below is
used to toggle on the theme name using JavaScript's turnary operator.

```js
function switchThemeName(currentThemeName) {
    return currentThemeName === 'light' ? 'dark' : 'light'
}
```

Now we just call this function whenever the `switchTheme` mutation is commited:

```js
PreferencesStore.mutations.switchTheme = function(state) {
    state.theme = switchThemeName(state.theme)
}
```


## Persistence

We the user's theme choice to persist across browsing sessions.


## Exports

Export the compiled store object.

```js
export default createStore.persist('preferences', PreferencesStore)
```
