# Assessment Store

This store provides a deterministic interface for communicating with the backend
medical diagnosis model. It uses [IndigoClient](../assets/modules/IndigoClient.js), which implements the actual API.


## Imports

```js
import { createStore } from '../framework/framework.js'
import settings from '../settings.js'
import IndigoClient from '../assets/modules/IndigoClient.js'
```


## Setup

Create a new store object:

```js
const AssessmentStore = {
    state: {},
    mutations: {},
    actions: {}
}
```

We also need to create a new API client instance:

```js
const client = new IndigoClient(settings.tokens.indigoAPIEndpoint)
```


## State

Initially, all fields are set to null, since this store is not used until
starting an assessment.


```js
AssessmentStore.state.medicalConditionDistribution = null
AssessmentStore.state.setMedicalConditionNames = null
AssessmentStore.state.modalMedicalCondition = null
AssessmentStore.state.question = 'Do you have a headache?'
```


## Mutations

Update the posterior distribution:

```js
AssessmentStore.mutations.setMedicalConditionDistribution = function(state, distribution) {
    state.medicalConditionDistribution = distribution
}
```

Update the list of possible condition names. This should be called only once
when starting the assessment.

```js
AssessmentStore.mutations.setMedicalConditionNames = function(state, medicalConditionNames) {
    state.medicalConditionNames = medicalConditionNames
}
```

Update the most likely (modal) medical condition, as given by the backend model.

```js
AssessmentStore.mutations.setModalMedicalCondition = function(state, modalMedicalCondition) {
    state.modalMedicalCondition = modalMedicalCondition
}
```

Update the question being asked.

```js
AssessmentStore.mutations.setQuestion = function(state, question) {
        state.question = question
}
```


## Actions

Since the API is asynchronous, we need to define several actions for this store.


This first action, initialises the medical condition distribution. It commits
`setMedicalConditionNames` and `setMedicalConditionDistribution`.


```js
// set the initial medical condition distribution.

AssessmentStore.actions.initMedicalConditionDistribution = function(context) {

    client
        .getPrior()
        .then(({ medicalConditionNames, medicalConditionPrior }) => {

            context.commit('setMedicalConditionDistribution', medicalConditionPrior)
            context.commit('setMedicalConditionNames', medicalConditionNames)
        })
}
```

Next, we define an action to *update* the posterior distribution once a new one
is received from the model.

```js
// change the medial condition distribution based on symptoms stated by
// patient.

AssessmentStore.actions.updateMedicalConditionDistribution = function(context, symptomsMap) {

    const priorDistribution = context.state.medicalConditionDistribution

    client
        .postPosterior(priorDistribution, symptomsMap)
        .then(({ posteriorDistribution, posteriorMode }) => {

            context.commit('setMedicalConditionDistribution', posteriorDistribution)
            context.commit('setModalMedicalCondition', posteriorMode)
        })
}
```

Finally, we define an action to update the question being asked, based on the user's
answer.

```js
AssessmentStore.actions.updateQuestion = function(context, answer) {

        client
                .getQuestion(answer)
                .then(({ question }) => {
                        context.commit('setQuestion', question)
                })
}
```


## Exports

```js
export default createStore(AssessmentStore)
```
