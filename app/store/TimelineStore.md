# Timeline Store


## Imports

```js
import { createStore } from '../framework/framework.js'
```


## Setup

```js
const TimelineStore = {
    state: {},
    mutations: {}
}
```


## State


```js
TimelineStore.state.timeline = []
```


## Mutations

The only mutation needed is a settter for `isFirstUse`. Note that `!!value`
ensures the updated state is a Boolean.

```js
TimelineStore.mutations.addToTimeline = function(state, { condition, symptoms, evidence, date }) {
    state.timeline.push({
            condition,
            symptoms,
            evidence,
            date
    })
}
```

```js
TimelineStore.mutations.removeFromTimeline = function(state, index) {

    state.timeline = [
            ...state.timeline.slice(0, index - 1),
            ...state.timeline.slice(index)
    ]
}
```


## Persistence

By definition, `isFirstUse` needs to persist accross browsing sessions.


## Exports

```js
export default createStore.persist('timeline', TimelineStore)
```
