import { swipe, swipeInverse, fade, fadeInverse, duration } from './routerAnimations.js'



export default class Router {

        constructor({ default, ...routes }) {
                this.routes = routes
                this.route = null
                this.default = default
                this.routeNames = Object.keys(routes)
                this.middlewares = []

                this.element = document.createElement('div')
                this.outlet = document.createElement('div')
                this.style = document.createElement('style')
                this.shadow = this.element.attachShadow({ mode: 'open' })

                this.shadow.append(style)
                this.shadow.append(outlet)

                style.innerText = ``

                startElement = routes[start](routerContext)
                this.outlet.append(startElement)
        }


        use(middleware) {
                this.middlewares.push(middleware)
        }


        runMiddlewares(nextRoute) {

                for (let middleware of this.middlewares) {
                        nextRoute = middleware({ destination: nextRoute })
                }

                return nextRoute
        }


        push(nextRouteName, attributes, animation=swipe) {

                nextRouteName = this.runMiddlewares(nextRouteName)

                const nextRenderFunction = routes[nextRouteName]
                const nextElement = nextRenderFunction(attributes)
                const lastElement = this.outlet.children[this.outlet.children.length - 1]

                lastElement.animate(animation.last.keyframes, animation.last.timing)
                nextElement.animate(animation.next.keyframes, animation.next.timing)

                this.outlet.appendChild(nextElement)
                this.route = nextRouteName
                observable(nextRouteName)
        }


        pop(animation=swipeInverse) {
                const nextElement = this.outlet.children[this.outlet.children.length - 1]
                const lastElement = this.outlet.children[this.outlet.children.length - 2]


                lastElement.animate(animation.last.keyframes, animation.last.timing)
                nextElement.animate(animation.next.keyframes, animation.next.timing)

                setTimeout(() => {
                    this.outlet.removeChild(nextElement)
                }, duration)
        }


        to(nextRouteName, attributes, animation=swipe) {
                this.push(nextRouteName, animation)

                setTimeout(() => {

                    for (let child of [...this.outlet.children].slice(0, -1)) {
                        this.outlet.removeChild(child)
                    }
                }, duration)
        }

}


export default function (...a) {
        return new Router(...a)
}
