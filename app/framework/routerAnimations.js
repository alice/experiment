export const duration = 500


const fadeIn = {
    keyframes: [
        { filter: 'opacity(0)' },
        { filter: 'opacity(1)' },
    ],

    timing: {
        duration: duration / 3,
        fill: 'forwards',
        easing: 'ease-in-out'
    }
}


const fadeOut = {
    keyframes: [
        { filter: 'opacity(1)' },
        { filter: 'opacity(0)' },
    ],

    timing: {
        duration: duration / 3,
        fill: 'forwards',
        easing: 'ease-in-out'
    }
}


export const fade = {
    next: fadeIn,
    last: fadeOut
}

export const fadeInverse = {
    next: fadeOut,
    last: fadeIn
}



const swipeBack = {
    keyframes: [
        { transform: 'translateX(0)', filter: 'opacity(1)' },
        { transform: 'translateX(-300px)', filter: 'opacity(0)' }
    ],

    timing: {
        duration: duration,
        fill: 'forwards',
        easing: 'linear'
    }
}


const swipeFront = {
    keyframes: [
        { transform: 'translateX(100vw)' },
        { transform: 'translateX(0vw)' }
    ],

    timing: {
        duration: duration,
        fill: 'forwards',
        easing: 'cubic-bezier(.18,.71,.25,.98)'
    }
}


export const swipe = {
    next: swipeFront,
    last: swipeBack,
}


export const swipeInverse = {
    next: {
        keyframes: [
            { transform: 'translateX(0vw)' },
            { transform: 'translateX(100vw)' },
        ],

        timing: {
            duration: duration / 1.5,
            fill: 'forwards',
            easing: 'ease-in-out'
        }
    },

    last: {
        keyframes: [
            { transform: 'translateX(-110px)', filter: 'opacity(0)' },
            { transform: 'translateX(0)', filter: 'opacity(1)' },
        ],

        timing: {
            duration: duration / 1.5,
            fill: 'forwards',
            easing: 'ease-in-out'
        }
    }
}
