export function fiber(value) {
	const users = []
	const fiber = v => v ? relay(value = v) : value
	const relay = v => users.forEach(user => user(value))
        fiber.isFiber = true
	fiber.on = user =>
		user(value, users.push(user))
	return fiber
}


Object.prototype.on=function(u){u(this)}

function bindAttributes(root, attributes={}) {

        for (let attributeName of Object.keys(attributes)) {
                let attributeValue = attributes[attributeName]

		if (attributeName.startsWith('$')) {
			attributeValue.on(value => root[attributeName.slice(1)] = value)
		} else {
	                attributeValue.on(value => root.setAttribute(attributeName, value))
	                root.__defineGetter__(attributeName, () => root.getAttribute(attributeName))
		}
        }
}


export function createComponent({ template, styles, methods, data, attributes }) {


        return givenAttributes => {
		const element = document.createElement('div')
		let root = element

		Object.assign(element, methods, data)
		Object.assign(element, { template, styles })
                root.innerHTML = ''


                bindAttributes(root, givenAttributes)

                if (styles) {
                        root = element.attachShadow({ mode: 'open' })
                        let styleElement = document.createElement('style')

                        styleElement.textContent = element.styles.bind(element)()
                        root.appendChild(styleElement)
                }

                if (template) {
                        root.append(element.template.bind(element)())
                }

                return element
        }
}


createComponent.styled = function(styleFunction, defaultAttributes) {


        return (first, ...rest) => {
		const element = document.createElement('div')
		const shadow = element.attachShadow({ mode: 'open' })
		const styleElement = document.createElement('style')


		if (defaultAttributes) bindAttributes(element, defaultAttributes)
                shadow.innerHTML = ''

                if (first instanceof HTMLElement || !(typeof(first) == 'object')) {

                        shadow.append(first, ...rest)

                } else if (typeof(first) == 'object')  {
                        const {child, ...attributes} = first

                        bindAttributes(element, attributes)

			if (child) child.on(v => {
				//
				// shadow.innerHTML = ''
				// shadow.append(v)

				const currentChild = shadow.querySelector(':not(style)')

				if (typeof(v) == 'string') {
					let s = v
					v = document.createElement('span')
					v.textContent = s
				}

				if (currentChild) {
					//shadow.replaceNode(v, currentChild)
					shadow.removeChild(currentChild)
					shadow.append(v)
				} else {
					shadow.append(v)
				}
			})
                }

		styleElement.textContent = styleFunction.bind(element)()
		shadow.appendChild(styleElement)
                return element
        }
}


export function fragment(...children) {
        const element = document.createElement('div')
        element.append(...children)

        return element
}

export function native(is, props, ...children) {
	const element = document.createElement(is)

	bindAttributes(element, props)
	element.append(...children)

	return element
}



const duration = 500


const fadeIn = {
    keyframes: [
        { filter: 'opacity(0)' },
        { filter: 'opacity(1)' },
    ],

    timing: {
        duration: duration / 3,
        fill: 'forwards',
        easing: 'ease-in-out'
    }
}


const fadeOut = {
    keyframes: [
        { filter: 'opacity(1)' },
        { filter: 'opacity(0)' },
    ],

    timing: {
        duration: duration / 3,
        fill: 'forwards',
        easing: 'ease-in-out'
    }
}


export const fade = {
    next: fadeIn,
    last: fadeOut
}

const fadeInverse = {
    next: fadeOut,
    last: fadeIn
}



const swipeBack = {
    keyframes: [
        { transform: 'translateX(0)', filter: 'opacity(1)' },
        { transform: 'translateX(-300px)', filter: 'opacity(0)' }
    ],

    timing: {
        duration: duration,
        fill: 'forwards',
        easing: 'linear'
    }
}


const swipeFront = {
    keyframes: [
        { transform: 'translateX(100vw)' },
        { transform: 'translateX(0vw)' }
    ],

    timing: {
        duration: duration,
        fill: 'forwards',
        easing: 'cubic-bezier(.18,.71,.25,.98)'
    }
}


const swipe = {
    next: swipeFront,
    last: swipeBack,
}


const swipeInverse = {
    next: {
        keyframes: [
            { transform: 'translateX(0vw)' },
            { transform: 'translateX(100vw)' },
        ],

        timing: {
            duration: duration / 1.5,
            fill: 'forwards',
            easing: 'ease-in-out'
        }
    },

    last: {
        keyframes: [
            { transform: 'translateX(-110px)', filter: 'opacity(0)' },
            { transform: 'translateX(0)', filter: 'opacity(1)' },
        ],

        timing: {
            duration: duration / 1.5,
            fill: 'forwards',
            easing: 'ease-in-out'
        }
    }
}

class Router {

        constructor({ start, ...routes }) {
                this.routes = routes
                this.route = null
                this.routeNames = Object.keys(routes)
                this.middlewares = []
		this.start = start
		this.listeners = []
        }

	outlet() {
                // this.element = document.createElement('div')
                this._outlet = document.createElement('div')
		this._outlet.style = 'width:100%;position:relative;top:0;left:0;'
                // this.style = document.createElement('style')
                // this.shadow = this.element.attachShadow({ mode: 'open' })

                // this.shadow.append(style)
                // this.shadow.append(this._outlet)
		const start = this.runMiddlewares(this.start)

                let startElement = this.routes[start]()
                this._outlet.append(startElement)


		return this._outlet
	}

	fiber() {
		return {
			isFiber: true,
			on: listener => {
				this.listeners.push(listener)
			},
		}
	}

	notifyListeners(routeName) {
		this.listeners.forEach(listener => listener(routeName))
	}


        use(middleware) {
                this.middlewares.push(middleware)
        }


        runMiddlewares(nextRoute) {

                for (let middleware of this.middlewares) {
                        nextRoute = middleware({ destination: nextRoute })
                }

		this.notifyListeners(nextRoute)

                return nextRoute
        }


        push(nextRouteName, attributes, animation=swipe) {

                nextRouteName = this.runMiddlewares(nextRouteName)

                const nextRenderFunction = this.routes[nextRouteName]
                const nextElement = nextRenderFunction(attributes)
                const lastElement = this._outlet.children[this._outlet.children.length - 1]

                lastElement.animate(animation.last.keyframes, animation.last.timing)
                nextElement.animate(animation.next.keyframes, animation.next.timing)

                this._outlet.appendChild(nextElement)
                this.route = nextRouteName
        }


        pop(animation=swipeInverse) {
                const nextElement = this._outlet.children[this._outlet.children.length - 1]
                const lastElement = this._outlet.children[this._outlet.children.length - 2]


                lastElement.animate(animation.last.keyframes, animation.last.timing)
                nextElement.animate(animation.next.keyframes, animation.next.timing)

                setTimeout(() => {
                    this._outlet.removeChild(nextElement)
                }, duration)
        }


        to(nextRouteName, attributes, animation=swipe) {
                this.push(nextRouteName, attributes, animation)

                setTimeout(() => {

                    for (let child of [...this._outlet.children].slice(0, -1)) {
                        this._outlet.removeChild(child)
                    }
                }, duration)
        }

}


export function createRouter(...a) {
        return new Router(...a)
}


class Store {

    constructor(initialState, mutations, actions) {

        this.listeners = {}
        this.state = {...initialState}
        this.mutations = mutations
        this.actions = actions
    }


    getStateClone() {
        return {...this.state}
    }


    subscribe(mutationName, listener) {

        if (! this.listeners.hasOwnProperty(mutationName)) {
            this.listeners[mutationName] = []
        }

        this.listeners[mutationName].push(listener)
    }


    notify(mutationName) {
        this.listeners[mutationName]?.forEach(listener =>
            listener(this.getStateClone())
        )
    }


    commit(mutationName, ...mutationArgs) {

        if (this.mutations.hasOwnProperty(mutationName)) {

            const mutate = this.mutations[mutationName]
            const stateClone = this.getStateClone()

            mutate(stateClone, ...mutationArgs)

            this.state = stateClone
            this.notify(mutationName)
        }
    }


    dispatch(actionName, ...actionArgs) {

        if (this.actions.hasOwnProperty(actionName)) {

            const action = this.actions[actionName]
            action(this, ...actionArgs)
        }
    }

    fiber(mutationName, variableName) {
	    return {
		    isFiber: true,
		    on: listener => {

			    this.subscribe(mutationName, state => listener(state[variableName]))
			    listener(this.state[variableName])
		    }
	    }
    }
}


export function createStore({ state, mutations, actions }) {

    return new Store(
        state,
        mutations,
        actions
    )
}


createStore.persist = function(storeName, storeTemplate) {

	storeTemplate = {...storeTemplate}

	const storedState = localStorage.getItem(storeName)
	if (storedState !== null) storeTemplate.state = JSON.parse(storedState)

	const store = createStore(storeTemplate)

	Object.keys(store.mutations).forEach(mutationName => {
		store.subscribe(mutationName, state => {
			localStorage.setItem(storeName, JSON.stringify(state))
		})
	})

	return store
}
