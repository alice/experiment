# Activities Screen

This screen is the default screen shown within the home page. It provides a list
of activities for the user to try out.


## Imports


```js
import { createComponent, fragment } from '../framework/framework.js'
import activityCardStrings from '../assets/strings/activityCardStrings.js'
import Router from '../router/index.js'
import Heading from '../components/system/Heading.js'
import Text from '../components/system/Text.js'
import NavigationCard from '../components/system/NavigationCard.js'
```


## Template

```js
const App = {

        template() {
                return fragment(
                        ...activityCardStrings.map(props =>
                                NavigationCard({
                                        $router: Router,
                                        ...props
                                })
                        )
                )
        }
}

/*
return fragment(
        ...activityCardStrings.map(props =>
                NavigationCard({
                        $router: Router,
                        ...props
                })
        )
)
*/
```


## Exports

```js
export default createComponent(App)
```
