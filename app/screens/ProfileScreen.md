# Profile Screen

The profile screen allows the user to manage preferences (such as the app theme), change their details and export their data.


## Imports

```js
import { createComponent, fragment } from '../framework/framework.js'
```

## Setup

```js
const App = {}
```

## Styles

```js
```

## Template

```js
App.template = function() {
        return fragment(
                'this is the profile screen'
        )
}
```


## Exports

```js
export default createComponent(App)
```
