```js
import settings from '../settings.js'
import IndigoClient from '../assets/modules/IndigoClient.js'
import { createComponent, fragment, fiber } from '../framework/framework.js'
import Router from '../router/index.js'
import Heading from '../components/system/Heading.js'
import Text from '../components/system/Text.js'
import InputArea from '../components/system/InputArea.js'
import Button from '../components/system/Button.js'
import Screen from '../components/system/Screen.js'
```


```js
const App = {}

const answer = fiber('')
const client = new IndigoClient(settings.tokens.indigoAPIEndpoint)
```

## Template

```js
App.template = function() {

        return Screen(
                Heading({
                        size: 3,
                        bold: true,
                        child: 'Quick Search'
                }),
                Text('Describe how you\'re feeling below and we\'ll try to find the closest matching conditions or illnesses.'),
                InputArea({
                        $fiber: () => answer
                }),
                Button({
                        child: 'Search',
                        $onclick: () => {

                                client.searchConditions(answer())
                                        .then(results => Router.push('searchResults', {
                                                $results: results              
                                        }))
                        }
                })
        )
}
```


## Exports

```js
export default createComponent(App)
```
