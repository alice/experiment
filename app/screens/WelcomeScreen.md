# Welcome Screen

This screen provides onboarding pages for app. These pages are intended to be
shown to users on first use of the app. Each onboarding page features a heading,
image, explanatory message, and button leading to the next slide. These properties
are defined in [assets/strings/onboardingPageStrings.js](../assets/strings/onboardingPageStrings.js).


## Imports

```js
import { createComponent, fragment } from '../framework/framework.js'
import onboardingPageStrings from '../assets/strings/onboardingPageStrings.js'
import settings from '../settings.js'
import UserStore from '../store/UserStore.js'
```


Import components:

```js
import Router from '../router/index.js'
import Heading from '../components/system/Heading.js'
import Text from '../components/system/Text.js'
import Image from '../components/system/Image.js'
import Button from '../components/system/Button.js'
import Screen from '../components/system/Screen.js'
```


## Setup

```js
const App = {}
```


## Attributes

This route takes an optional `index` attribute which specifies the onboarding
page to show. The default value is 0. Using an index in this way allows the
next-page-along to be easily routed to from the current oage via Fiber's
Router API.

```js
App.attributes = [ 'index' ]
```


## Methods

```js
App.methods = {}
```

\
\
This method routes to the next onboarding page.

```js
App.methods.routeToNextPage = function() {

        console.log('going to next page')

        Router.push('welcome', {
                index: this.idx + 1
        })
}
```

\
\
This method route's to the app's homepage, concluding the onboarding process.
Note that `Router.to()` is used rather `Router.push()`. This ensures elements
used for onboarding are removed from the DOM tree.

```js
App.methods.routeToHomePage = function() {

        UserStore.commit('negateFirstUse')
        Router.to(settings.homepage)
}
```

\
\
This method checks if a next onboarding page exists.

```js
App.methods.nextPageExists = function() {

        return this.idx + 1 < onboardingPageStrings.length
}
```

\
\
This method responds to when the the continue button is pressed. If a next
onboarding page exists, it'll call `routeToNextPage()`, or otherwise
`routeToHomepage()`.

```js
App.methods.handleContinue = function() {

        return this.nextPageExists()
                ? this.routeToNextPage()
                : this.routeToHomePage()
}
```

\
\
This method returns the content for an onboarding page at a particular index.
```js
App.methods.getOnboardingPageContent = function() {
        return onboardingPageStrings[this.idx]
}
```


## Styles

The welcome screens are styled in a typical way, with large clear widgets. The
continue button is placed low down on the screen to allow for easy access on
mobile. The image is made large and centred to create a more emersive experience.

The desired positioning can be achieved quite automatically using a flexbox with
`justify-content: space-around`. This allows for greater adaptivity to different
screen sizes.


```js
App.styles = function() {

        return `
                :host > div {

                        display: flex;
                        flex-direction: column;
                        justify-content: space-around;
                }
        `
}
```


## Template

This template renders the image, heading, text and button.


```js
App.template = function() {

        this.idx = +this.index || 0
        const content = this.getOnboardingPageContent()

        return Screen(
                Image({
                        url: content.image,
                        scale: 70,
                        style: 'margin-top:8rem;'
                }),
                fragment(
                        Heading({
                                bold: true,
                                child: content.heading,
                        }),
                        Text({
                                child: content.text,
                        }),
                        Button({
                                child: content.button ?? 'next',
                                $onclick: this.handleContinue.bind(this)
                        })
                )
        )                
}     
```


## Exports

```js
export default createComponent(App)
```
