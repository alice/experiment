```js
import { createComponent, fragment, fiber } from '../framework/framework.js'
import Router from '../router/index.js'
import Heading from '../components/system/Heading.js'
import Text from '../components/system/Text.js'
import InputArea from '../components/system/InputArea.js'
import Button from '../components/system/Button.js'
import Screen from '../components/system/Screen.js'
import Card from '../components/system/Card.js'
import Scrollable from '../components/system/Scrollable.js'
```


```js
const App = {}
```


## Methods

```js
App.methods = {}
```


```js
App.methods.renderResult = function({ name, sentence, score }) {

        return Card(
                Heading({
                        size: 6,
                        bold: true,
                        child: name
                }),
                Text(sentence.slice(0, 100) + '...'),
                Button({
                        child: 'Learn more',
                        $onclick() {
                                window.open(`https://www.nhs.uk/search/results?q=${name}`)
                        }
                })
        )
}
```


## Template

```js
App.template = function() {

        return Screen(
                Heading({
                        size: 3,
                        bold: true,
                        child: 'Results'
                }),
                Scrollable(
                        Text('Based on your description, we found these similar conditions:'),
                        fragment(
                                ...this.results.map(this.renderResult.bind(this))
                        )
                )
        )
}
```


## Exports

```js
export default createComponent(App)
```
