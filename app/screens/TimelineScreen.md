# Timeline Screen

The timeline screen is intended to provide users with an overview and historical
record of their medical interviews. Currently, this simply lists and summarises
their past interviews, allowing them to press on any of them and view the results again. However in future, we hope to add more analysis or visualisation
features.


## Imports

```js
import { createComponent, fragment } from '../framework/framework.js'
import TimelineStore from '../store/TimelineStore.js'
import Heading from '../components/system/Heading.js'
import Screen from '../components/system/Screen.js'
import Card from '../components/system/Card.js'
import Text from '../components/system/Text.js'
```

## Setup

```js
const App = {}
```

## Styles


## Template

```js
App.template = function() {
        return fragment(
                Text('Revisit past results at any time.'),

                ...TimelineStore.state.timeline.map(item => {
                        return Card(
                                Heading({
                                        size: 6,
                                        bold: true,
                                        child: item.date
                                }),
                                Text(`Condition: ${item.condition}`),
                        )
                })
        )
}


                // Card(
                //         Heading({
                //                 size: 6,
                //                 bold: true,
                //                 child: '31 Mar 2022'
                //         }),
                //         Text('Result: common cold')
                // ),
                // Card(
                //         Heading({
                //                 size: 6,
                //                 bold: true,
                //                 child: '23 Mar 2022'
                //         }),
                //         Text('Result: common cold')
                // ),
                // Card(
                //         Heading({
                //                 size: 6,
                //                 bold: true,
                //                 child: '18 Mar 2022'
                //         }),
                //         Text('Result: flu, hey fever')
                // ),
```


## Exports

```js
export default createComponent(App)
```
