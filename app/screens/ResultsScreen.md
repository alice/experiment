# Results Screen

The results screen is used to display the results and evaluation/conclusion of
a medical interview. This is used both for viewing past interview results, and
for the most recent interview. It aims to provide a clear, unambiguous and
visually appealing presentation of the possible conditions a user may be suffering from. It also makes explicit any caveats. From this screen, users can
also download their results in PDF or HTML format. This allows the results to
be shared easily via their devices built-in mechanisms.


## Imports

```js
import { createComponent } from '../framework/framework.js'
```

## Setup

```js
const App = {}
```

## Styles

```js
```

## Template

```js
App.template = function() {

        window.open(this.searchURL)
}
```


## Exports

```js
export default createComponent(App)
```
