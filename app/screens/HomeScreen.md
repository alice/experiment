# Home Screen

The home screen is the app's homepage screen. It render the main view and the
footer/navigation bar. Note that it uses a seperate router object to the main
app router. This is neccessary to decouple the footer from the screen being
show, while also allowing *some* screens to be footerless.


## Imports

```js
import { createComponent } from '../framework/framework.js'
import { HomeRouter } from '../router/index.js'
import Header from '../components/Header.js'
import Footer from '../components/Footer.js'
import TabsLayout from '../components/system/TabsLayout.js'
import Screen from '../components/system/Screen.js'
```


## Template

```js
const App = {
        template() {

                return Screen(
                        TabsLayout(
                                Header(),
                                HomeRouter.outlet(),
                                // Footer()
                        )
                )
        }
}
```


## Exports

```js
export default createComponent(App)
```
