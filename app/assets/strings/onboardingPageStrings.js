export default [
    {
        heading: 'Meet Ava 👋',
        text: 'Ava is a simple chatbot designed to check your symptoms.',
        image: '/assets/images/heart.png',
    },
    {
        heading: 'Discuss',
        text: 'Discuss your symptoms in short medical interviews.',
        image: '/assets/images/plus.png',
    },
    {
        heading: 'Evaluate',
        text: 'Gain insights about possible causes, and generate health reports.',
        image: '/assets/images/isometric.png',
    },
    {
        heading: 'Share',
        text: 'Easily download and share your results with peers.',
        image: '/assets/images/signal.png',
        button: 'Get Started'
    },
]
