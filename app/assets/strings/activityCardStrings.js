export default [
        {
                heading: '2 Minute Interview',
                text: 'Take a short medical interview with Ava.',
                route: 'assessment',
                image: '/assets/images/heart.png'
        },
        {
                heading: 'Quick Search',
                text: 'Describe your symptoms and find matching conditions.',
                route: 'search',
                image: '/assets/images/piechart.png'
        },
]
