export default [
        {
                type: 'open',
                heading: 'Let\'s start with your main symptoms 😷',
                text: 'Please briefly describe some of your main symptoms, e.g. "I have a headache."',
        },
        {
                type: 'options',
                heading: 'How long have you been having these symptoms?',
                exclusive: true,
                options: [
                        'Less than a day',
                        'Between a day and a week',
                        '1-2 weeks',
                        'A month or more'
                ]
        },
        {
                type: 'options',
                heading: 'Are your symptoms improving, worsening or staying the same?',
                exclusive: true,
                options: [
                        'Improving',
                        'Worsening',
                        'Staying the same'
                ]
        },
        {
                type: 'options',
                heading: 'How severe are your symptoms?',
                text: 'Tip: mild symptoms have little effect on your daily activities.',
                exclusive: true,
                options: [
                        'Mild',
                        'Moderate',
                        'Severe'
                ]
        },
        {
                type: 'open',
                heading: 'Next let\'s get down to some specifics 👩‍⚕️',
                text: 'Please describe where the worst symptom is located.',
        },
        ...Array(3).fill(0).map(() => ({

                type: 'model',
                heading: 'Are you experiencing any of the following?',
        })),
        ...Array(9).fill(0).map(() => ({

                type: 'binary'
        })),
        {
                type: 'results',
                heading: 'Results',
                text: 'Thanks for your answers. Based on your symptoms, we think you might have the following condition:'
        }
]
