export function argmax(array) {
        return array.map((x, i) => [x, i]).reduce((r, a) => (a[0] > r[0] ? a : r))[1];
}

export function shuffle(array) {
        return array.sort(() => Math.random() < 0.5 ? -1 : 1)
}
