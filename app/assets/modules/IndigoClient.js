const priorRoute = 'prior'
const posteriorRoute = 'posterior'
const searchRoute = 'search'


class IndigoClient {

    constructor(endpoint) {
        this.endpoint = endpoint
    }


    async request(method, route, body, headers={}) {

        const url = this.getURL(route)

        const response = await fetch(url, {
            mode: 'cors',
            method,
            headers,
            body,
        })

        return response.json()
    }


    get(...rest) {
        return this.request('GET', ...rest)
    }


    post(...rest) {
        return this.request('POST', ...rest)
    }


    getURL(route) {
        return this.endpoint + '/' + route
    }


    getPrior() {
        return this.get(priorRoute)
    }


    postPosterior(priorDistribution, symptomsMap) {

        const data = new FormData()

        data.append('priorDistribution', JSON.stringify(priorDistribution))
        data.append('symptomsMap', JSON.stringify(symptomsMap))

        return this.post(posteriorRoute, data)
    }


    searchConditions(symptomsDescription) {

            const data = new FormData()
            data.append('paragraph', symptomsDescription)

            return this.post(searchRoute, data)
    }


    getQuestion(previousAnswer=null) {
            return this.get(`question${previousAnswer !== null ? `?answer=${previousAnswer}` : ''}`)
    }
}


export default IndigoClient
