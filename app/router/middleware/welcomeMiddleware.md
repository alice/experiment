# Welcome Middleware

This router middware is responsible for deciding whether the [welcome screen](../screens/WelcomeScreen.md)
is shown to the user upon opening the app. Currently the welcome screen should
only appear on first use. After using app `isFirstUse` in [UserStore](../store/UserStore.md)
will be persistently set to `false`.


## Imports

```js
import UserStore from '../../store/UserStore.js'
```


## Logic

The guard logic is simple, check if `isFirstUse` is set to `true`, and if so,
route to the welcome screen. Otherwise, just continue to the original destination.

```js
function welcomeMiddleware({ destination }) {

	return UserStore.state.isFirstUse ? 'welcome' : destination
}
```

## Exports

```js
export default welcomeMiddleware
```
