# Router Configuration

This file is responsible for setting up the two routers used in the app. For
those not familiar with client-side routing, we provide a brief explanation:

Thick-client JavaScript apps such as this, implement a virtual navigation
layer over the browser's existing URL-based system. The reason for doing so is
to allow for more control over how different screens are loaded, apply custom
animations, and create a more coherent implementation (i.e. everything is just
a component). In simple websites it's usually sufficient to have a single
global router. In our case however, we require a router *stack*. The home
screen contains navigable sub-screens, so we need a separate router to handle
it. In our app we have two routers overall - one for the top level, generically
called `Router`, and another for the home screen, called `HomeRouter`.


## Imports

```js
import { createRouter } from '../framework/framework.js'
import welcomeMiddleware from './middleware/welcomeMiddleware.js'
```

Now let's import all of our screens:

```js
import HomeScreen from '../screens/HomeScreen.js'
// import ProfileScreen from '../screens/ProfileScreen.js'
import ResultsScreen from '../screens/ResultsScreen.js'
import TimelineScreen from '../screens/TimelineScreen.js'
import WelcomeScreen from '../screens/WelcomeScreen.js'
import ActivitiesScreen from '../screens/ActivitiesScreen.js'
import AssessmentScreen from '../screens/AssessmentScreen.js'
import SearchScreen from '../screens/SearchScreen.js'
import SearchResultsScreen from '../screens/SearchResultsScreen.js'
```


## Home Router

```js
const HomeRouter = createRouter({
        start: 'activities',
        activities: ActivitiesScreen,
        timeline: TimelineScreen,
        // profile: ProfileScreen,
})
```


## General Router

```js
const Router = createRouter({
        start: 'home',
        home: HomeScreen,
        welcome: WelcomeScreen,
        results: ResultsScreen,
        assessment: AssessmentScreen,
        search: SearchScreen,
        searchResults: SearchResultsScreen
})
```

The main app router utilises `welcomeMiddleware` to ensure that onboarding
screens are shown the user when first using the app.

```js
Router.use(welcomeMiddleware)
```


## Exports

```js
export { HomeRouter }
export default Router
```
