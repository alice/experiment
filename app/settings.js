export default {
        homepage: 'home',
        theme: {
                default: 'dark'
        },
        tokens: {
                indigoAPIEndpoint: 'http://localhost:5000/'
        }
}
