# Header Component

This component implements the header on [Home Screen](../screens/HomeScreen.md).
It's a fixed position widget which shows the title of the current route.

## Imports

```js
import { createComponent } from '../framework/framework.js'
import { HomeRouter } from '../router/index.js'
import Heading from './system/Heading.js'
import Text from './system/Text.js'
```

## Setup

```js
const App = {}
```

## Styles

TODO

```js
App.styles = function() {

        return `
                :host {
                        text-transform: capitalize;
                        font-size: 1rem;
                }
        `
}
```

## Template

```js
App.template = function() {

        return Heading({
                size: 3,
                bold: true,
                child: HomeRouter.fiber(),
        })
}
```


## Exports

```js
export default createComponent(App)
```
