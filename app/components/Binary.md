# Binary Component


## Imports

```js
import { createComponent, fragment } from '../framework/framework.js'
import InputSelect from './system/InputSelect.js'
import AssessmentStore from '../store/AssessmentStore.js'
```


## Setup

```js
const App = {
        methods: {}
}
```


## Methods

This method updates the quesiton being asked based on the user's response.

```js
App.methods.updateQuestion = function(answer) {

        const present = !!answer().Yes

        AssessmentStore.dispatch('updateMedicalConditionDistribution', { [AssessmentStore.state.question]: present })
        AssessmentStore.dispatch('updateQuestion', present)
}
```



## Template

```js
App.template = function() {
        this.update.call = this.updateQuestion.bind(this)


        return fragment(
                InputSelect({
                        $exclusive: true,
                        $options: ['Yes', 'No'],
                        $fiber: this.fiber,
                })
        )
}
```


## Exports

```js
export default createComponent(App)
```
