# Icon Component

This component is used to render app icons, given as SVG images. The component
takes an attribute `name` which specifies the codename of the icon. The icon naming convention matches that used by Google's Material Icons.


## Imports

```js
import { createComponent, native } from '../../framework/framework.js'
```

## Setup

```js
const App = {}
```


## Styles

These styles were taken from Google's material icons stylesheet. To avoid
making unneccessary network requests, we've placed the styles in the component
directly.

```js
App.styles = function() {

        return `
                span {
                        font-family: 'Material Icons';
                        font-weight: normal;
                        font-style: normal;
                        font-size: 27px;
                        line-height: 1;
                        letter-spacing: normal;
                        text-transform: none;
                        display: inline-block;
                        white-space: nowrap;
                        word-wrap: normal;
                        direction: ltr;
                        -webkit-font-smoothing: antialiased;
                        width: min-content;
                }
        `
}
```


## Template

This template creates a span element with the icon name inside. The styles above use Google's Material Icons font to render this name as an SVG icon rather
than text.

```js
App.template = function() {

        return native('span', {}, this.icon)
}
```


## Exports

```js
export default createComponent(App)
```
