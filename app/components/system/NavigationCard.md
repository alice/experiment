# Navigation Card Component

This component uses `Card`, `Heading` and `Button` to implement a card widget
that allows users to navigate to another screen. This widget aims to make it
clear *where* the user will be taken and for what purpose. The button control
should be meaningfully named.


## Imports

```js
import { createComponent, fragment } from '../../framework/framework.js'
import Card from './Card.js'
import Heading from './Heading.js'
import Button from './Button.js'
import Text from './Text.js'
import Image from './Image.js'
```


## Setup

```js
const App = {}
```


## Attributes

This component takes several attributes limited to:

* `router` - a `Router` object to use for instigating navigation.
* `route` - the route to navigate to when the button is clicked.
* `heading` - the heading text to show.
* `text` - the summary text to show.
* `button` - the button text to show (default 'Start').
* `image` - an image to show.


## Methods

This method is used to respond to when the butotn is clicked. It simply takes
the given router object and instagates a stacked navigation to the given
route.


```js
App.methods = {}
```


```js
App.methods.handleClick = function() {

        this.router.push(this.route, this.routeAttributes)
}
```


## Template

```js
App.template = function() {
        return Card(
                this.image ? Image({
                        url: this.image,
                        style: 'margin-bottom:20px'
                }) : '',
                Heading({
                        size: 4,
                        child: this.heading
                }),
                Text({
                        inline: true,
                        child: this.text
                }),
                Button({
                        small: true,
                        child: this.button || 'Start',
                        $onclick: this.handleClick.bind(this)
                })
        )
}
```


## Exports

```js
export default createComponent(App)
```
