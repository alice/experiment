# Input Area Component

This component is used to provide a text-area input field.


## Imports

```js
import { createComponent, native } from '../../framework/framework.js'
```


## Setup

```js
const App = {}
```

## Styles

```js
App.styles = function() {

        return `
                :host > textarea {
                        border: none;
                        outline: none;
                        width: -webkit-fill-available;
                        min-height: 7rem;
                        background: var(--card);
                        resize: none;
                        color: var(--text);
                        font: inherit;
                        font-size: 0.9rem;
                        padding: var(--padding-base);
                        border-radius: 1rem;
                }
        `
}
```

## Template


```js
App.template = function() {

        const textarea = native('textarea')

        textarea.oninput = () => this.fiber()(textarea.value)
        return textarea
}
```

## Exports

```js
export default createComponent(App)
```
