# Tabs Layout Component

This component implements a generic tabs layout with a header, footer and main
page. The key requirements of this layout are that the header and footer
remain fixed, while the main content is allowed to scroll below.


## Imports

```js
import { createComponent } from '../../framework/framework.js'
```

# Styles

The implementation here is subject to change as it's quite messy and brittle.
Overall, this ensures that the header, footer and main content are all
positioned correctly on the page.

```js
const App = () => `

        :host {
                width: -webkit-fill-available;
        }

        /* Applied to the header */

        :host > :nth-child(1) {
                height: 6vh;
                overflow: hidden;
        }


        /* Applied to the main content */

        :host > :nth-child(2) > div {

                position: absolute;
                z-index: 3;

                left: 0;
                top: 0;

                width: 100%;
                height: 83vh;
                max-height: 83vh;

                overflow: scroll;
                padding: unset;
        }


        /* Applied to the footer */

        :host > :nth-child(3) {

                position: absolute;
                z-index: 3;

                left: 0;
                bottom: 30px;

                width: 100%;
                height: 10vh;

                background: #16161e;
                padding-top: 0.5rem;
        }
`
```


## Exports

```js
export default createComponent.styled(App)
```
