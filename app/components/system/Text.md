# Text Component

The text component is an atomic component used to display text. It can either
be used to show inline text or blocked paragraphs.


## Imports

```js
import { createComponent } from '../../framework/framework.js'
```

## Attributes

This component takes a Boolean attribute `inline` which is used to switch the
display mode to from `block` to `inline`.


## Styles

Not much styling is needed here. We simply set the font color to that specified
in the app theme, and optimize for text ledgibility/clarity.

```js
const App = () => {

        return `
                :host {
                        color: var(--text);
                        padding-bottom: calc(3 * var(--padding-base));


                        /* Optimize for readability. */

                        text-rendering: optimizeLegibility;
                        font-weight: 400;
                }

                :host([inline]) {
                        padding-bottom: 14px;
                }
        `
}
```

## Exports

```js
export default createComponent.styled(App)
```
