# Image Component

This component is used to display images within the app. Note that this does not
include icons, which are instead handled by the [Icon](./Icon.md) component.

## Imports

```js
import { createComponent, native } from '../../framework/framework.js'
```

## Setup

```js
const App = {}
```


## Attributes

It takes in an attribute `url` which is used to specify the imgae asset's
location. It also takes in `scale`, which is used to specify the image width.
Note that this also scales the hight.


## Styles

```js
App.styles = function() {

        return `
                img {
                        display: block;
                        width: ${this.scale || 30}%;


                        /* Center the image. */

                        margin: 0 auto;
                }
        `
}
```


## Template

This template simply returns a native image element with the `src` attribute
set to the given `url` value.

```js
App.template = function() {

        return native('img', { src: this.url })
}
```


## Exports

```js
export default createComponent(App)
```
