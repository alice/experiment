# Card Component

This component is used to create a card wrapper around an arbitrary child. This is useful for highlighting particular parts of the UI.


## Imports

```js
import { createComponent } from '../../framework/framework.js'
```


## Styles

```js
const App = () => `
        :host {
                background: var(--card); /*#151522*/

                display: block;
                width: -webkit-fill-available;

                border-radius: 16px;
                padding: var(--padding-base);
                margin-bottom: var(--padding-base);
                margin-top: var(--padding-base);
        }
`
```


## Exports

```js
export default createComponent.styled(App)
```
