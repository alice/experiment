# Scrollable Component

This component provides a generic scroll container. It attempts to take up the
full space available, and ensures any overflow content is scrolled.

## Improts

```js
import { createComponent } from '../../framework/framework.js'
```

## Styles

```js
const App = () => `
        :host {
                width: 100%;
                height: 100%;
                width: -webkit-fill-available;
                height: -webkit-fill-available;
                overflow-y: scroll;
        }
`
```

## Exports

```js
export default createComponent.styled(App)
```
