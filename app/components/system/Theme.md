# Theme Component

This component is used to provide the app's themes. The theme could have been
implemented via JavaScript, but we choose to use CSS3 variables instead, for the
sake of simplicity. Which theme styles are activated depends on the state of
`theme` in [PreferencesStore](../store/PreferencesStore.md).


# Imports

```js
import { createComponent, fiber } from '../../framework/framework.js'
import PreferencesStore from '../../store/PreferencesStore.js'
```


## Styles

Many of the values used here come from Apple's IOS design guides.

```js
const App = () => `
        :host {
                --red: rgb(255, 59, 48);
                --orange: rgb(255, 149, 0);
                --yellow: rgb(255, 204, 0);
                --green: rgb(52, 199, 89);
                --mint: rgb(0, 199, 190);
                --teal: rgb(48, 179, 199);
                --cyan: rgb(50, 173, 230);
                --blue: rgb(10, 132, 255);
                --indigo: rgb(94, 92, 230);
                --purple: rgb(175, 82, 222);
                --pink: rgb(255, 55, 95);
                --brown: rgb(172, 142, 104);
                --black: rgb(0, 0, 0);
                --white: rgb(255, 255, 255);
                --gray-1: rgb(142, 142, 147);
                --gray-2: rgb(99, 99, 102);
                --gray-3: rgb(72, 72, 74);
                --gray-4: rgb(58, 58, 60);
                --gray-5: rgb(44, 44, 46);
                --gray-6: rgb(28, 28, 30);
                --gray-7: rgb(142, 142, 147);
                --gray-8: rgb(174, 174, 178);
                --gray-9: rgb(199, 199, 204);
                --gray-10: rgb(209, 209, 214);
                --gray-11: rgb(229, 229, 234);
                --gray-12: rgb(242, 242, 247);

                --heading-1: 2.53rem;
                --heading-2: 2.08rem;
                --heading-3: 1.64rem;
                --heading-4: 1.49rem;
                --heading-5: 1.27rem;
                --heading-6: 1.12rem;
                --heading-7: 0.89rem;

                --font-size-base: var(--heading-6);
                --font-family: 'Inter', system-ui, sans-serif;
                --font-line-height: 1.4;

                --padding-screen: 1.7rem;
                --padding-base: 1rem;
        }


        /* Light theme. */

        :host-context(#light) {
                --primary: var(--blue);
                --background: var(--gray-12);
                --text: var(--gray-6);
                --card: var(--white);
        }


        /* Dark theme. */

        :host-context(#dark) {
                --primary: var(--white);
                --secondary: var(--blue);
                --background: var(--black);
                --text: var(--gray-7);
                --card: #0e0e16;
                --button: rgb(14 99 255);
        }
`
```


## Exports

Here we compile the component as a styled component, and bind the attribute `id`
to the current theme preference.

```js
export default createComponent.styled(App, {
        id: PreferencesStore.fiber('switchTheme', 'theme')
})
```
