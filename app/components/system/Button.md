# Button Component

This component implements a simple button.

## Imports

```js
import { createComponent } from '../../framework/framework.js'
```


# Styles

The main aspects of this stylesheet are setting the button's background colour,
and invoking an animation when the button is clicked.


```js
const App = () => `
        :host {
                background: var(--button);
                color: var(--primary);

                font-weight: 600;
                font-size: 1rem;
                text-align: center;
                text-transform: capitalize;

                border-radius: 13px;
                padding: 17px;

                cursor: pointer;
                transition: box-shadow .5s ease-in-out;
        }

        :host([small]) {
                padding: 13px;
        }
`
```


## Exports

```js
export default createComponent.styled(App)
```
