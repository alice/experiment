# Heading Component

This component is used to standardise headings accross the app. It takes in a
`size` attribute, which determins how large the heading is.


## Imports

```js
import { createComponent } from '../../framework/framework.js'
```


## Styles

```js
const App = function() {

        return `
                :host {
                        padding-bottom: var(--padding-base);
                        color: var(--primary);
                        font-size: var(--heading-${this.size || 1});
                }

                :host([bold]) {
                        font-weight: 700;
                }
        `
}
```


## Exports

We export it as a styled component.

```js
export default createComponent.styled(App)
```
