# Screen Component

The screens component is used as a container for all app screens, which handles
positioning and scaling. This removes the need for each screen to individually
(and possibly inconsistently) implement these aspects. This component makes
no assumptions about its parent other than that it's a block element with a
defined width and height. A screen should take up the full width and height of
the parent.


## Imports

```js
import { createComponent } from '../../framework/framework.js'
```


## Styles

```js
const App = () => `
        :host {
                background: var(--background);
                padding: var(--padding-screen);


                /* Attempt to take up the full screen. */

                height: 100vh;
                width: 100vw;
                width: -webkit-fill-available;


                /* Work nicely with router outlets. */

                position: absolute;
                top: 0;
                left: 0;
        }
`
```


## Exports

```js
export default createComponent.styled(App)
```
