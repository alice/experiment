# Scaffold

This component is responsible for applying the styles provided by `Theme`. It
also ensures the app is proportioned correctly on different devices. This
responsivity is implemented using various CSS3 media queries.


## Imports

```js
import { createComponent } from '../../framework/framework.js'
```


## Styles

The styles here are grouped according to role. The first section uses the CSS
variables defined by `Theme` and actually applies them to the view. The third
section ensures that the app takes up all available screen space. A caveat here
is that `100vh` on mobile Safari includes the bottom bar, meaning *our* app will
run off screen. To fix this, we use `-webkit-fill-available` if it's supported
instead.


```js
const App = () => `
        :host {
                /* Apply theme. */

                background: var(--background);
                color: var(--text);
                font: var(--font-size-base)/var(--font-line-height) var(--font-family);
                font-weight: 500;


                /* Use a generic transition to soften harsh movements. */

                /* transition: all .4s; */


                /* Prevent scrolling, zooming and other unneccessary movements. */

                position: fixed;
                overflow: hidden;
                top: 0;
                left: 0;


                /* Make sure we're taking up the entire viewport height. */

                max-height: 100vh;
                max-height: -webkit-fill-available;
                max-width: 100vw;
                width: 100vw;
                height: 100vh;
                height: -webkit-fill-available;


                /* Center children horizontally. */

                display: flex;
                flex-direction: column;
                align-items: center;
        }


        :host > * {

                /* To improve desktop and tablet viewing, restrict children to a
                width of 600 viewport pixels. */

                max-width: 600px;
        }
`
```


## Exports

```js
export default createComponent.styled(App)
```
