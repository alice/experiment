# Input Select Component

This component provides a simple multiple-choice selection input.


## Imports

```js
import { createComponent, fragment, native } from '../../framework/framework.js'
import Heading from './Heading.js'
```


## Setup

```js
const App = {}
```


## Styles

```js
App.styles = function() {

        return `
                :host #option {
                        border: 2px solid #323246;
                        margin-bottom: 1rem;
                        border-radius: 0.7rem;
                        background: var(--card);
                        transition: background .2s ease-out;
                }

                :host #option.set {
                        background: #282146;
                }

                :host #option > div {
                        margin-left: 0.5rem;
                        margin-top: 1rem;
                        font-weight: 600;
                        text-transform: unset;
                }
        `
}
```

## Methods

This method renders and individual option. The options have a header and
optional text, and are highlighted once selected.

```js
App.methods = {}
```

This method handles option clicks. It feeds back the selected option to the
provided fiber variable.

```js
App.methods.handleClick = function(index) {

        const options = this.shadowRoot.querySelectorAll('#option')

        if (this.exclusive) {
                options.forEach(node => node.classList.remove('set'))
        }

        options[index].classList.toggle('set')
        this.feedBack(options)

}
```

```js
App.methods.feedBack = function(options) {

        const fiber = this.fiber()
        const mapping = Object.fromEntries(

                [...options]
                        .map(node => [
                                node.children[0].shadowRoot.querySelector('span').innerText,
                                node.className == 'set'
                        ])
        )

        fiber(mapping)
}
```


```js
App.methods.renderOption = function(text, i) {

        return native('div', { id: 'option'},
                Heading({
                        size: 6,
                        child: text,
                        $onclick: () => {
                                this.handleClick(i)
                        }
                })
        )
}
```


## Template

```js
App.template = function() {

        return fragment(
                ...this.options.map(this.renderOption.bind(this))
        )
}
```


## Exports

```js
export default createComponent(App)
```
