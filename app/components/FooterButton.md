# Footer Button Component

This component implements an individual footer button. It takes a route name.
This are used to route to the desired location when the button is clicked.

## Imports

```js
import { createComponent, fiber, fade, fragment, native } from '../framework/framework.js'
import { HomeRouter } from '../router/index.js'
import Icon from './system/Icon.js'
```


## Setup

```js
const App = {}
```


## Attributes

As mentioned, this component takes a route name. It also takes an icon name.


```js
App.attributes = [ 'route', 'icon' ]
```


## Methods

This method is responsible for responding to when the icon is clicked. It simply routes to the desired location.

```js
App.methods = {

        handleClick() {
                HomeRouter.to(this.route, {}, fade)
        }
}
```


## Styles


```js
App.styles = function() {

        return `
                :host {
                        transition: color .4s ease-out;
                        color: var(--primary);
                        width: fit-content;
                }

                :host(:hover) {
                        color: var(--secondary);
                }

                :host > div {
                        display: flex;
                        align-items: center;
                        flex-direction: column;
                }

                span {
                        color: var(--text);
                        font-size: 0.7rem;
                        text-transform: capitalize;
                }
        `
}
```


## Template

```js
App.template = function() {

        return fragment(
                Icon({
                        icon: this.icon,
                        $onclick: this.handleClick.bind(this)
                }),
                native('span', {}, this.route)
        )
}
```

## Exports

```js
export default createComponent(App)
```
