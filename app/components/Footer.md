# Footer Component

This component implements a bottom navigation bar, allowing the user to easily
switch between screens.


## Imports

```js
import footerIconStrings from '../assets/strings/footerIconStrings.js'
import { createComponent, fragment } from '../framework/framework.js'
import Router, { HomeRouter } from '../router/index.js'
import Icon from './system/Icon.js'
import FooterButton from './FooterButton.js'
```

## Setup

```js
const App = {}
```


## Styles

This styles make the footer icons render horizontally.

```js
App.styles = function() {

        return `
                :host > div {
                        display: flex;
                        justify-content: space-around;
                        width: 90%;
                        padding-bottom: var(--padding-base);
                        margin: 0 auto;
                }
        `
}
```


## Template

This template runs through each route name and maps it to a `FooterButton`
component. The footer icons are also given dynamically.

```js
App.template = function() {

        return fragment(
                ...HomeRouter.routeNames.map(
                        route => FooterButton({
                                route,
                                icon: footerIconStrings[route]
                        })
                )
        )
}
```


## Exports

```js
export default createComponent(App)
```
