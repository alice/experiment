# App Component

This component is the top level component for the app. Every other component,
including screens, render inside of it. It's main role is to aggregate providers
and perform any high level initialisation. As of now, the only provider is
[Theme](./Theme.md) and we don't need to initialise anything.


## Imports

```js
import { createComponent } from '../framework/framework.js'
import Router from '../router/index.js'
import Scaffold from './system/Scaffold.js'
import Theme from './system/Theme.js'
```


## Template

```js
const App = {

        template() {
                return Theme(
                        Scaffold(
                                Router.outlet()
                        )
                )        
        }
}
```


## Exports

```js
export default createComponent(App)
```
