# Interview Component

This component is responsible for conducting medical interviews with users. It
uses `AssessmentStore` to send the users's symptoms to the backend model, and
feed back results. The main user experience requirement for this component is
fluidity. That is, we aim to make the interview process feel smooth and
continuous rather than consisting of harsh cuts. This is achieved via CSS
animations and by restricting the number of widgets on screen at any given time. This should allow the user to more easily focus on the content/questions
being presented to them.


## Imports

```js
import { createComponent, fragment } from '../../framework/framework.js'
import { argmax, shuffle } from '../assets/modules/math.js'
import AssessmentStore from '../store/AssessmentStore.js'
import InputSelect from './system/InputSelect.js'
import Heading from './system/Heading.js'
```


## Setup

```js
const App = {
        methods: {}
}
```

Initialise the model.

```js
AssessmentStore.dispatch('initMedicalConditionDistribution')
setTimeout(() => AssessmentStore.dispatch('updateMedicalConditionDistribution', {'sneezing': true}), 1500)
```


## Styles

No styles yet.


## Methods

```js
App.methods = {}
```

This method renders the symptoms of the modal medical condition as an options
input.

```js
App.methods.renderModalConditionSymptoms = function() {

        const [ condition, symptoms ] = AssessmentStore.state.modalMedicalCondition
        const $options = shuffle(symptoms).slice(0, 3)

        return InputSelect({
                $options,
                $fiber: this.fiber
        })
}
```

This method updates the posterior distribution based on the user's response.

```js
App.methods.updatePosterior = function(answer) {
        
        AssessmentStore.dispatch('updateMedicalConditionDistribution', answer())
}
```

## Template

```js
App.template = function() {
        this.update.call = this.updatePosterior.bind(this)

        return fragment(
                Heading({
                        size: 3,
                        bold: true,
                        child: 'Are you experiencing any of the following?'
                }),
                this.renderModalConditionSymptoms()
        )
}
```


## Exports

```js
export default createComponent(App)
```
